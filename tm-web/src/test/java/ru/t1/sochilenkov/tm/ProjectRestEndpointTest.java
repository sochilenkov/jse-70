package ru.t1.sochilenkov.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.client.ProjectRestEndpointClient;
import ru.t1.sochilenkov.tm.dto.ProjectDTO;
import ru.t1.sochilenkov.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @NotNull
    final ProjectDTO projectManual = new ProjectDTO("Project MANUAL TEST");

    @NotNull
    final ProjectDTO projectAutoFirst = new ProjectDTO("Project Test 1");


    @Before
    public void init() {
        client.post(projectAutoFirst);
    }

    @After
    public void clear() {
        client.delete(projectAutoFirst.getId());
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get(projectAutoFirst.getId()));
        Assert.assertThrows(FeignException.class, () -> client.get(projectManual.getId()));
    }

    @Test
    public void testPost() {
        Assert.assertThrows(FeignException.class, () -> client.get(projectManual.getId()));
        client.post(projectManual);
        Assert.assertNotNull(client.get(projectManual.getId()));
        client.delete(projectManual.getId());
    }

    @Test
    public void testPut() {
        Assert.assertNull(client.get(projectAutoFirst.getId()).getDescription());
        projectAutoFirst.setDescription("New Description");
        client.put(projectAutoFirst);
        Assert.assertEquals("New Description", client.get(projectAutoFirst.getId()).getDescription());
        projectAutoFirst.setDescription(null);
        client.put(projectAutoFirst);
        Assert.assertNull(client.get(projectAutoFirst.getId()).getDescription());
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get(projectAutoFirst.getId()));
        client.delete(projectAutoFirst.getId());
        Assert.assertThrows(FeignException.class, () -> client.get(projectAutoFirst.getId()));
        client.post(projectAutoFirst);
    }

}
