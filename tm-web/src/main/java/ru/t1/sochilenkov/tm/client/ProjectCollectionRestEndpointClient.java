package ru.t1.sochilenkov.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.sochilenkov.tm.dto.ProjectDTO;

import java.util.List;

public interface ProjectCollectionRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/projects";

    static ProjectCollectionRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectCollectionRestEndpointClient.class, BASE_URL);
    }

    @Nullable
    @GetMapping()
    List<ProjectDTO> get();

    @PostMapping
    void post(@NotNull @RequestBody List<ProjectDTO> projects);

    @PutMapping
    void put(@NotNull @RequestBody List<ProjectDTO> projects);

    @DeleteMapping()
    void delete(@NotNull @RequestBody List<ProjectDTO> projects);

}
