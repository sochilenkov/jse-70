package ru.t1.sochilenkov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.http.auth.AuthenticationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sochilenkov.tm.api.service.model.IProjectService;
import ru.t1.sochilenkov.tm.exception.IdEmptyException;
import ru.t1.sochilenkov.tm.exception.NameEmptyException;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.model.User;
import ru.t1.sochilenkov.tm.repository.model.IProjectRepository;
import ru.t1.sochilenkov.tm.repository.model.IUserRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    @Transactional
    @SneakyThrows
    public void save(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null) throw new AuthenticationException();
        if (project == null) throw new IdEmptyException();
        if (project.getName() == null || project.getName().isEmpty()) throw new NameEmptyException();
        @Nullable final User user = userRepository.findById(userId).get();
        project.setUser(user);
        repository.save(project);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void saveAll(@Nullable final String userId, @Nullable final Collection<Project> projects) {
        if (userId == null) throw new AuthenticationException();
        if (projects == null) throw new IdEmptyException();
        if (projects.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = userRepository.findById(userId).get();
        for (@NotNull Project projectDTO : projects) {
            projectDTO.setUser(user);
            if (projectDTO.getName() == null || projectDTO.getName().isEmpty()) throw new NameEmptyException();
        }
        repository.saveAll(projects);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null) throw new AuthenticationException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId, @Nullable final Collection<Project> projects) {
        if (userId == null) throw new AuthenticationException();
        repository.deleteAll(projects);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOne(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null) throw new AuthenticationException();
        if (project == null) throw new IdEmptyException();
        repository.deleteByIdAndUserId(project.getId(), userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null) throw new AuthenticationException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getProjectNameById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId).getName();
    }

}
