package ru.t1.sochilenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.sochilenkov.tm.api.service.dto.IProjectDTOService;
import ru.t1.sochilenkov.tm.api.service.model.IProjectService;
import ru.t1.sochilenkov.tm.dto.ProjectDTO;
import ru.t1.sochilenkov.tm.model.CustomUser;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint {

    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping("/{id}")
    public ProjectDTO get(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        return projectDTOService.findOneById(user.getUserId(), id);
    }

    @PostMapping
    public void post(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody ProjectDTO project) {
        projectDTOService.save(user.getUserId(), project);
    }

    @PutMapping
    public void put(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @RequestBody ProjectDTO project) {
        projectDTOService.save(user.getUserId(), project);
    }

    @DeleteMapping("/{id}")
    public void delete(
            @AuthenticationPrincipal final CustomUser user,
            @NotNull @PathVariable("id") String id) {
        projectService.removeOneById(user.getUserId(), id);
    }

}
