package ru.t1.sochilenkov.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.sochilenkov.tm.dto.ProjectDTO;

public interface ProjectRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/project";

    static ProjectRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @Nullable
    @GetMapping("/{id}")
    ProjectDTO get(@NotNull @PathVariable("id") String id);

    @PostMapping
    void post(@NotNull @RequestBody ProjectDTO project);

    @PutMapping
    void put(@NotNull @RequestBody ProjectDTO project);

    @DeleteMapping("/{id}")
    void delete(@NotNull @PathVariable("id") String id);

}
