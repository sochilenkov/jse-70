<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT LIST</h1>

<table width="100%", cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap" align="left">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="150" nowrap="nowrap" align="center">STATUS</th>
        <th width="100" nowrap="nowrap" align="center">CREATED</th>
        <th width="100" nowrap="nowrap" align="center">FINISH</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th width="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>

    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td align="center" nowrap="nowrap">
                <c:out value="${project.status.displayName}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.created}"/>
            </td>
            <td align="center">
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}"/>
            </td>
            <td>
                <a href="/project/edit/${project.id}">EDIT</a>
            </td>
            <td>
                <a href="/project/delete/${project.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin: 20px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
