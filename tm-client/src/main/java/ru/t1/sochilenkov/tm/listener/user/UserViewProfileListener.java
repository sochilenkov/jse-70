package ru.t1.sochilenkov.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;
import ru.t1.sochilenkov.tm.dto.request.UserProfileRequest;
import ru.t1.sochilenkov.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final UserDTO user = authEndpoint.profile(request).getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE_NAME NAME: " + user.getMiddleName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
