package ru.t1.sochilenkov.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sochilenkov.tm.listener.LoggerListener;
import ru.t1.sochilenkov.tm.service.ReceiverService;

@NoArgsConstructor
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    @NotNull
    @Autowired
    private LoggerListener loggerListener;

    @SneakyThrows
    public void start() {
        receiverService.receive(loggerListener);
    }

}
