package ru.t1.sochilenkov.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
